﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnRadius : MonoBehaviour
{
    public GameObject Enemy;
    public Transform center;
    public Vector3 size;

    public void Spawn()
    {
        Vector3 pos = center.position + new Vector3(Random.Range(-size.x / 2, size.x / 2), 0, Random.Range(-size.z / 2, size.z / 2));
        Instantiate(Enemy, pos, Quaternion.identity);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1, 0, 0, 0.5f);
        Gizmos.DrawCube(center.position, size);
    }
}

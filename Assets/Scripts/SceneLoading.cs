﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoading : MonoBehaviour
{
    private int sceneToLoad;

    void Start()
    {
        GameObject gameManger = GameObject.Find("GM");
        NextScene setActiveScene = gameManger.GetComponent<NextScene>();
        sceneToLoad = setActiveScene.nextSceneToLoad;
        StartCoroutine(LoadAsyncOperation());   
    }


    IEnumerator LoadAsyncOperation()
    {
        AsyncOperation gameLevel = SceneManager.LoadSceneAsync(sceneToLoad);
        yield return new WaitForEndOfFrame();
    }
}


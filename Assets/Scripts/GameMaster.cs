﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster : MonoBehaviour
{
    private static GameMaster instance;
    private GameObject player;
    private Vector3 lastCheckPoint;


    public static GameMaster Instance
    {
        get
        {
            if (instance)
                return instance;
            else
                return new GameObject("GM").AddComponent<GameMaster>();
        }
    }
   
    private void Awake() //so only one GameMaster can exist
    {
         if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    public void SetPlayer(GameObject _player)
    {
        player = _player;
    }

    public void SetLastCheckPoint(Vector3 _position)
    {
        lastCheckPoint = _position;
    }

    public void WarpToLastCheckpoint()
    {
        player.transform.position = lastCheckPoint;
        Physics.SyncTransforms();
    }

}

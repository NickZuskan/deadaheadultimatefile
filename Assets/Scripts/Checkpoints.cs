﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoints : MonoBehaviour
{
   
    void OnTriggerEnter(Collider other) //sets the players checkpoint 
     {
        if (other.tag == "ControllerWithHead")
        {
            
            GameMaster.Instance.SetLastCheckPoint(transform.localPosition);

        }
       
     }
}
